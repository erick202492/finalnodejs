const db = require("../config/db.js");
const config = require("../config/config.js");
const Comen = db.comments;
const asyncMiddleware = require("express-async-handler");

exports.addComen = asyncMiddleware(async (req, res) => {
    
    await Comen.create({
        comen: req.body.comen,
        userId: req.userId,
        //bookId: req.body.bookId
        bookId: req.params.id
    });

    res.status(201).send({
        status: "Comments has been created!"
    });
});

exports.viewComen = asyncMiddleware(async (req, res) => {
    const comen = await Comen.findAll();
    res.status(200).json({
        description: "View Comen",
        data: comen
    });
});

exports.viewComenByBook = asyncMiddleware(async (req, res) => {
    const comen = await Comen.findAll({
        where: {
            bookID: req.params.id
        }
    }
    );
    res.status(200).json({
        description: "View One Coment",
        data: comen
    });
});
