const db = require("../config/db.js");
const config = require("../config/config.js");
const Book = db.book;
const Comen = db.comments;
const User = db.user;
const asyncMiddleware = require("express-async-handler");

exports.addBook = asyncMiddleware(async (req, res) => {
    // Save User to Database
    console.log("Processing func -> SignUp");

    await Book.create({
        title: req.body.title,
        author: req.body.author,
        published_date: req.body.published_date,
        pages: req.body.pages,
        language: req.body.language,
        publisher_id: req.body.publisher_id,
    });

    //   const roles = await Role.findAll({
    //     where: {
    //       name: {
    //         [Op.or]: req.body.roles
    //       }
    //     }
    //   });

    //   await user.setRoles(roles);
    //   await user.setRoles(roles, {through : {status : "unblock"}});

    res.status(201).send({
        status: "Books has been created!"
    });
});

exports.viewBook = asyncMiddleware(async (req, res) => {
    const book = await Book.findAll();
    res.status(200).json({
        description: "View Books",
        data: book
    });
});

exports.viewBookId = asyncMiddleware(async (req, res) => {
    const book = await Book.findAll({
        where: {
            id: req.params.id
        }
    }
    );
    res.status(200).json({
        description: "View One Books",
        data: book
    });
});

exports.viewBookId = asyncMiddleware(async (req, res) => {
    const book = await Book.findAll({
        where: {
            id: req.params.id
        }
    }
    );
    res.status(200).json({
        description: "View One Books",
        data: book
    });
});

exports.deleteBookId = asyncMiddleware(async (req, res) => {
    const book = await Book.destroy({
        where: {
            id: req.params.id
        }
    }
    );
    res.status(200).json({
        description: "Delete One Books",
        data: book
    });
});


exports.editBook = asyncMiddleware(async (req, res) => {
    // Save User to Database
    console.log("Processing func -> SignUp");

    await Book.update({
        title: req.body.title,
        author: req.body.author,
        published_date: req.body.published_date,
        pages: req.body.pages,
        language: req.body.language,
        publisher_id: req.body.publisher_id,
    },
        {
            where: {
                id: req.params.id
            }
        }
    );

    //   const roles = await Role.findAll({
    //     where: {
    //       name: {
    //         [Op.or]: req.body.roles
    //       }
    //     }
    //   });

    //   await user.setRoles(roles);
    //   await user.setRoles(roles, {through : {status : "unblock"}});

    res.status(201).send({
        status: "Books has been updated.!"
    });
});

exports.detailBook = asyncMiddleware(async (req, res) => {
    try {
      const detail = await Book.findAll({
        where: {
          id: req.params.id
        },
        attributes: [
          "id",
          "title",
          "author",
          "published_date",
          "pages",
          "language",
          "publisher_id"
        ],
        include: [
          {
            model: Comen,
            attributes: ["comen"],
            include: {
              model: User,
              attributes: ["username"]
            }
          }
        ]
      });
      res.status(200).json({
        description: "Book detail found!",
        data: detail
      });
    } catch (error) {
      res.status(500).send({
        error: error,
        message: "Book detail not fetched!"
      });
    }
  });



