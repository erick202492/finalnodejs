const db = require("../config/db.js");
const User = db.user;
const Role = db.role;
const Book = db.book;
const User_Roles = db.user_roles;
const asyncMiddleware = require("express-async-handler");
var bcrypt = require("bcryptjs");

exports.users = asyncMiddleware(async (req, res) => {
    const user = await User.findAll({
        attributes: ["id","name", "username", "email"],
        include: [
            {
                model: Role,
                attributes: ["id", "name"],
                through: {
                    attributes: ["userId", "roleId", "status"]
                }
            }
        ]
    });
    res.status(200).json({
        description: "All User",
        user: user
    });
});

exports.deleteUserId = asyncMiddleware(async (req, res) => {
    const user = await User.destroy({
        where: {
            id: req.params.id
        }
    }
    );
    res.status(200).json({
        description: "Delete One users",
        data: user
    });
});

exports.editRole = asyncMiddleware(async (req, res) => {
    const user_roles = await User_Roles.update(
      {
        roleId: req.body.roleId
      },
      {
        where: {
          userId: req.params.id
        }
      }
    );
    res.status(201).send({
      data: user_roles,
      message: "User role successfuly updated"
    });
  });

exports.editUser = asyncMiddleware(async (req, res) => {
    // Save User to Database
    
    //console.log(req.body.password);
    //console.log(req.params.id);

    const user = await User.findOne({  
            where: {
                id: req.userId
            }
        }  
    );

      if (!user) {
        return res.status(404).send({
    
          auth: false,
          accessToken: null,
          reason: "Id Not Found!"
        });
      }
      console.log(user.password)
    
      
       const passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
       console.log(passwordIsValid)
       if (!passwordIsValid) {
         return res.status(401).send({
           auth: false,
           accessToken: null,
           reason: "Invalid Password!"
         });
       }
    //   console.log(passwordIsValid);
    
      //const token = jwt.sign({ id: user.id }, config.secret, {
      //  expiresIn: 86400 // expires in 24 hours
      //});
    //   console.log(user.username);
    //   res.status(200).send({
    //     Username: user.username,
    //     Userid: user.id,
    //   });

    const updates = await User.update({
        name : req.body.name,
        username : req.body.username,
        email: req.body.email
    },
        {
            where: {
                id: req.userId
            }
        }
    );
    res.status(201).send({
        data : updates,
        status: "User has been updated.!"
    });
});

exports.usersbyid = asyncMiddleware(async (req, res) => {
    const user = await User.findOne({
        where: {
            id : req.params.id 
        }
        
    });
    res.status(200).json({
        description: "One User",
        user: user
    });
});



exports.userContent = asyncMiddleware(async (req, res) => {
    const user = await User.findOne({
        where: { id: req.userId },
        attributes: ["name", "username", "email"],
        include: [
            {
                model: Role,
                attributes: ["id", "name"],
                through: {
                    attributes: ["userId", "roleId"]
                }
            }
        ]
    });
    res.status(200).json({
        description: "User Content Page",
        user: user
    });
});

exports.adminBoard = asyncMiddleware(async (req, res) => {
    const user = await User.findOne({
        where: { id: req.userId },
        attributes: ["name", "username", "email"],
        include: [
            {
                model: Role,
                attributes: ["id", "name"],
                through: {
                    attributes: ["userId", "roleId"]
                }
            }
        ]
    });
    res.status(200).json({
        description: "Admin Board",
        user: user
    });
});

exports.managementBoard = asyncMiddleware(async (req, res) => {
    const user = await User.findOne({
        where: { id: req.userId },
        attributes: ["name", "username", "email"],
        include: [
            {
                model: Role,
                attributes: ["id", "name"],
                through: {
                    attributes: ["userId", "roleId"]
                }
            }
        ]
    });
    res.status(200).json({
        description: "Management Board",
        user: user
    });
});

exports.signup = asyncMiddleware(async (req, res) => {
  // Save User to Database
  console.log("Processing func -> SignUp");

  const user = await User.create({
    name: req.body.name,
    username: req.body.username,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8)
  });

  const roles = await Role.findAll({
    where: {
      name: {
        [Op.or]: req.body.roles
      }
    }
  });

  await user.setRoles(roles);
  await user.setRoles(roles, {through : {status : "unblock"}});

  res.status(201).send({
    status: "User registered successfully!"
  });
});


