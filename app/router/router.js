const verifySignUp = require("./verifySignUp");
const authJwt = require("./verifyJwtToken");
const authController = require("../controller/authController.js");
const userController = require("../controller/userController.js");
const bookController = require("../controller/bookController.js");
const orderController = require("../controller/orderController.js");
const comenController = require("../controller/comenController.js");

module.exports = function (app) {
  // Auth
  app.post(
    "/api/auth/signup",
    [
      verifySignUp.checkDuplicateUserNameOrEmail,
      verifySignUp.checkRolesExisted
    ],
    authController.signup
  );
  app.post("/api/auth/signin", authController.signin);
  app.put("/api/users/edit/", [
    authJwt.verifyToken
  ], userController.editUser);
  // get all user
  app.get("/api/users", [authJwt.verifyToken], userController.users);
  app.put("/api/users/roles/:id", [
    authJwt.verifyToken,
  ], userController.editRole);

  app.get("/api/users/:id", [authJwt.verifyToken], userController.usersbyid);
  app.delete("/api/users/:id", [authJwt.verifyToken], userController.deleteUserId);
  // get 1 user according to roles
  app.get("/api/test/user", [authJwt.verifyToken], userController.userContent);
  app.get(
    "/api/test/pm",
    [authJwt.verifyToken, authJwt.isPmOrAdmin],
    userController.managementBoard
  );
  app.get(
    "/api/test/admin",
    [authJwt.verifyToken, authJwt.isAdmin],
    userController.adminBoard
  );
  app.post("/api/books", [authJwt.verifyToken], bookController.addBook);
  app.get("/api/books", [authJwt.verifyToken], bookController.viewBook);
  app.get("/api/detailbooks/:id", [authJwt.verifyToken], bookController.detailBook);
  app.get("/api/books/:id", [authJwt.verifyToken], bookController.viewBookId);
  app.delete("/api/books/:id", [authJwt.verifyToken], bookController.deleteBookId);
  app.put("/api/books/:id", [authJwt.verifyToken], bookController.editBook);

  app.get("/api/comment/", [authJwt.verifyToken], comenController.viewComen);
  app.post("/api/comment/", [authJwt.verifyToken], comenController.addComen);
  app.post("/api/comment/:id", [authJwt.verifyToken], comenController.addComen);
  app.get("/api/comment/:id", [authJwt.verifyToken], comenController.viewComenByBook);

  app.post("/api/order/", [authJwt.verifyToken], orderController.addOrder);
  app.get("/api/order/", [authJwt.verifyToken], orderController.viewOrder);
  app.get("/api/order/:id", [authJwt.verifyToken], orderController.viewOrderId);
  

  // error handler 404
  app.use(function (req, res, next) {
    return res.status(404).send({
      status: 404,
      message: "Not Found"
    });
  });
  // error handler 500
  app.use(function (err, req, res, next) {
    return res.status(500).send({
      file: "/router/router.js",
      error: err
    });
  });
};
