const env = require('./env.js');
const Sequelize = require('sequelize');
const sequelize = new Sequelize(env.database, env.username, env.password, {
    host: env.host,
    dialect: env.dialect,
    logging: false,
    pool: {
        max: env.max,
        min: env.pool.min,
        acquire: env.pool.acquire,
        idle: env.pool.idle
    }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require('../model/user.js')(sequelize, Sequelize);
db.role = require('../model/role.js')(sequelize, Sequelize);
db.user_roles = require('../model/user_roles.js')(sequelize, Sequelize);
db.book = require('../model/book.js')(sequelize, Sequelize);
db.order = require('../model/order.js')(sequelize, Sequelize);
db.comments = require('../model/comen.js')(sequelize, Sequelize);

db.role.belongsToMany(db.user, {
    through: db.user_roles, foreignKey: 'roleId', otherKey: 'userId'
});
db.user.belongsToMany(db.role, {
    through: db.user_roles, foreignKey: 'userId', otherKey: 'roleId'
});

db.user.belongsToMany(db.book, {
     through: {model : 'order_books', unique : false}, foreignKey: 'userId', otherKey: 'bookId'
 });
db.book.belongsToMany(db.user, {
    through: {model :'order_books', unique : false}, foreignKey: 'bookId', otherKey: 'userId'
});

  db.user.hasMany(db.comments);
  db.comments.belongsTo(db.user);

 db.book.hasMany(db.comments);
  db.comments.belongsTo(db.book)

module.exports = db;